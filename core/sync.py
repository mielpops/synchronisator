import os

# We will need to change the current directory to sync books or not 
class Sync(object):
    def __init__(self, config: Config):
        self.config = config
        self.tablet_content = self.get_tablet_content()
        self.books_content = self.get_books_content()

    def get_tablet_content(self):
        return os.listdir(self.config.
        
